
var EventThumper = require('./lib/event_thumper.js');
var instance = undefined;

// As Singleton:
module.exports = {
  getInstance: function () {
    return instance || (instance = new EventThumper());
  }
};

