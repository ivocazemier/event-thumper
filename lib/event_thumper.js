var events = require('events');
var util = require('util');
var NanoTimer = require('nanotimer');


var EventThumper = function () {

  EventThumper.super_.call(this);

  // Private
  var self = this;

  var started = false;
  var thumpCounter = 0;

  var oneHunderedMillisecondsModulus = 1; // 100 ms
  var twoHunderedMillisecondsModulus = 2; // 200 ms
  var threeHunderedMilliSecondsModulus = 3; // 300 ms
  var fiveHunderedMillisecondsModulus = 5; // 500 ms
  var oneSecondModulus = 10; //  1000 ms

  // Public
  this.timerResolution = '100m'; // 10 milliseconds
  this.timer;

  this._100MS_ = '100msThump';
  this._200MS_ = '200msThump';
  this._300MS_ = '300msThump';
  this._500MS_ = '500msThump';
  this._1000MS_ = '1000msThump';


  this.thump = function () {

    // Resolution counter for delegating several events
    if (!thumpCounter) {
      thumpCounter = 0;
    }
    thumpCounter++;


    if (0 == (thumpCounter % oneHunderedMillisecondsModulus)) {
      self.emit(self._100MS_);
    }

    if (0 == (thumpCounter % twoHunderedMillisecondsModulus)) {
      self.emit(self._200MS_);
    }

    if (0 == (thumpCounter % threeHunderedMilliSecondsModulus)) {
      self.emit(self._300MS_);
    }

    if (0 == (thumpCounter % fiveHunderedMillisecondsModulus )) {
      self.emit(self._500MS_);
    }

    if (0 == (thumpCounter % oneSecondModulus )) {
      self.emit(self._1000MS_);
      thumpCounter = 0;
    }


  };

  this.startThumper = function (callback) {

    if (this.started) {
      return callback(null, 'already started the thumper');
    }
    thumpCounter = 0;
    started = true;
    this.timer = new NanoTimer();
    this.timer.setInterval(this.thump, null, this.timerResolution, function (err) {
      if (err) {
        if (callback) {
          callback(err, 'whoops an error occurred when we stopped the thumper');
        } else {
          console.log('whoops an error occurred when we stopped the thumper');
        }
      }
    });

  };

  this.stopThumper = function (callback) {

    if (!started) {
      if (callback) {
        return callback(null, 'already stopped the thumping');
      } else {
        console.log('already stopped the thumping');
        return null;
      }

    }

    started = false;
    thumpCounter = 0;
    this.timer.clearInterval();


    if (callback) {
      return callback(null, 'stopped thumper');
    } else {
      return null;
    }
  };


};

// Extending EventEmitter
util.inherits(EventThumper, events.EventEmitter);


module.exports = EventThumper;
