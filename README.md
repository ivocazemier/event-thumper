Just a startup of a EventEmitter extension which emits events on specific intervals.

Install: [npm install event-thumper](https://www.npmjs.com/package/event-thumper)